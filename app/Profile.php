<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    public $fillable = ['name', 'email', 'title', 'company', 'birthdate', 'notification', 'notification_time'];
}
