<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Profile;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProfileCreated;


class ProfileController extends Controller
{
    public function store(Request $req) {
        $data = (array)json_decode($req->getContent());

        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email|unique:profiles,email',
            'birthdate' => 'required|date_format:Y-m-d',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'errors' => $errors->all()
            ]);
        }

        $obj = new Profile;
        $obj->fill($data);
        $obj->save();
        $data['id'] = $obj->id;
        
        $time = '';

        if ($obj->notification) {
            $json = json_decode($obj->notification_time);
            $time = $json->interval . ' ' . $json->type;
        }

        $obj->notification_time = $time;

        Mail::to($data['email'])->send(new ProfileCreated($obj));

        \Log::info((new \App\Mail\ProfileCreated($obj))->render());
        return response()->json($data);        
    }

    public function show(Request $req, $id) {
        return Profile::find($id);
    }

    public function update(Request $req, $id) {
        $row = Profile::findOrFail($id);
        $data = (array)json_decode($req->getContent());

        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('profiles')->ignore($id),
            ],
            'birthdate' => 'required|date_format:Y-m-d',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'errors' => $errors->all()
            ]);
        }

        $row->update($data);
        return $row;
    }

    public function delete(Request $req, $id) {
        $row = Profile::findOrFail($id);
        $row->delete();

        return 204;
    }
}
