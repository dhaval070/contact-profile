<?php

use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
        'name' => $faker->name('male'),
        'email' => $faker->unique()->safeEmail,
        'company' => $faker->words(3, true),
        'title' => $faker->titleMale(),
        'birthdate' => $faker->date(),
        'notification' => 0,
    ];
});
