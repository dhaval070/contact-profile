<h3>Profile Created:</h3>
<table width="100%" border="1" cellspacing="3" cellpadding="3">
<thead>
<tr>
    <th>Name</th>
    <th>Email</th>
    <th>Company</th>
    <th>Title</th>
    <th>Birthdate</th>
    <th>Notification</th>
    <th>Notification Time</th>
</tr>
</thead>
<tbody>
    <tr>
        <td>{{$profile->name}}</td>
        <td>{{$profile->email}}</td>
        <td>{{$profile->company}}</td>
        <td>{{$profile->title}}</td>
        <td>{{$profile->birthdate}}</td>
        <td>{{$profile->notification ? 'Yes' : 'No'}}</td>
        <td>{{$profile->notification_time}}</td>
    </tr>
</tbody>
</table>

