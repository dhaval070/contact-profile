<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profile;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/profiles', function (Request $request) {
    return Profile::all();
});

Route::get('/profiles/{id}', 'ProfileController@show');
Route::post('/profiles', 'ProfileController@store');
Route::put('/profiles/{id}', 'ProfileController@update');
Route::delete('/profiles/{id}', 'ProfileController@delete');
